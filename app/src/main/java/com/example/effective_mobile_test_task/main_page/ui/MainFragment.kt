package com.example.effective_mobile_test_task.main_page.ui

import android.view.View
import com.example.effective_mobile_test_task.R
import com.example.effective_mobile_test_task.common.mvvm.BaseFragment
import com.example.effective_mobile_test_task.utils.showNavigationBottomMenu
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainFragment : BaseFragment(R.layout.fragment_main) {
    override fun bind() {

    }

    override fun initViews(view: View) {
        showNavigationBottomMenu()
    }
}