package com.example.effective_mobile_test_task.root

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.effective_mobile_test_task.R
import com.example.effective_mobile_test_task.basket.BasketFragment
import com.example.effective_mobile_test_task.catalog.ui.fragment.CatalogFragment
import com.example.effective_mobile_test_task.databinding.ActivityRootBinding
import com.example.effective_mobile_test_task.login.ui.LoginFragment
import com.example.effective_mobile_test_task.main_page.ui.MainFragment
import com.example.effective_mobile_test_task.profile.ProfileFragment
import com.example.effective_mobile_test_task.promotion.PromotionFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RootActivity : AppCompatActivity() {
    private val binding: ActivityRootBinding by viewBinding()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_root)
        supportActionBar?.hide()
//        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        // Инициализируем sharedPreferences
        val sharedPreference = getSharedPreferences("LOGIN_PREF", Context.MODE_PRIVATE)
        // Имя пользователя
        val firstName = sharedPreference.getString("FIRSTNAME", "")
        // Фамилия пользователя
        val lastName = sharedPreference.getString("LASTNAME", "")
        // Номер телефона пользователя
        val phoneNumber = sharedPreference.getString("PHONE_NUMBER", "")


        // Если все значения из sharedPreference не пустые строки, то вас сразу перенаправляют на главную страницу
        if (firstName != "" && lastName != "" && phoneNumber != "") {
            replace(MainFragment())
        }

        // Если хоть одно поле пустое, направляют на страницу авторизации (нужно заново логиниться)
        else replace(LoginFragment())

        binding.bottomNavigation.setOnItemSelectedListener { item ->
            when(item.itemId) {
                R.id.main_page -> {
                    replace(MainFragment())
                    true
                }
                R.id.catalog_page -> {
                    replace(CatalogFragment())
                    true
                }
                R.id.basket_page -> {
                    replace(BasketFragment())
                    true
                }
                R.id.promotion_page -> {
                    replace(PromotionFragment())
                    true
                }
                R.id.profile_page -> {
                    replace(ProfileFragment())
                    true
                }
                else -> false
            }
        }
    }

    private fun replace(fragment: Fragment){
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, fragment)
            .addToBackStack(null) // Добавление в back stack для обратной навигации
            .commit()
    }
}