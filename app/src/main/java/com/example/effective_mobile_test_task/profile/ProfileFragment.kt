package com.example.effective_mobile_test_task.profile

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import androidx.fragment.app.viewModels
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.effective_mobile_test_task.R
import com.example.effective_mobile_test_task.catalog.ui.viewmodel.CatalogViewModel
import com.example.effective_mobile_test_task.common.mvvm.BaseFragment
import com.example.effective_mobile_test_task.databinding.FragmentProfileBinding
import com.example.effective_mobile_test_task.login.ui.LoginFragment
import com.example.effective_mobile_test_task.main_page.ui.MainFragment
import com.example.effective_mobile_test_task.utils.replace

class ProfileFragment : BaseFragment(R.layout.fragment_profile) {
    private val binding: FragmentProfileBinding by viewBinding()
    private val viewModel: CatalogViewModel by viewModels()

    override fun bind() {
        showUserInfo()
    }

    override fun initViews(view: View) {
        with(binding) {
            logoutButton.setOnClickListener {
                val sharedPreference =
                    activity?.getSharedPreferences(
                        "LOGIN_PREF",
                        Context.MODE_PRIVATE
                    )

                // Начинаем его редактировать
                val editor = sharedPreference?.edit()
                val firstName = "" // Обнуляем имя пользователя
                val lastName = "" // Обнуляем фамилию пользователя
                val phoneNumber = "" // Обнуляем номер телефона пользователя
                editor?.putString(
                    "FIRSTNAME",
                    firstName
                ) // Передаем в sharedPreferences firstname
                editor?.putString("LASTNAME", lastName) // Передаем в sharedPreferences lastName
                editor?.putString(
                    "PHONE_NUMBER",
                    phoneNumber
                ) // Передаем в sharedPreferences phoneNumber
                editor?.apply() // Применяем изменения
                replace(LoginFragment()) // Отправляем пользователя на страницу авторизации
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun showUserInfo() {
        // Инициализируем sharedPreferences
        val sharedPreference = activity?.getSharedPreferences("LOGIN_PREF", Context.MODE_PRIVATE)
        // Имя пользователя
        val firstName = sharedPreference?.getString("FIRSTNAME", "")
        // Фамилия пользователя
        val lastName = sharedPreference?.getString("LASTNAME", "")
        // Номер телефона пользователя
        val phoneNumber = sharedPreference?.getString("PHONE_NUMBER", "")

        with(binding) {
            userFullName.text = "$firstName $lastName"
            userPhoneNumber.text = "+ ${phoneNumber?.substring(0, 1)}" +
                    " ${phoneNumber?.substring(1, 4)} ${phoneNumber?.substring(4, 7)}" +
                    " ${phoneNumber?.substring(7, 9)} ${phoneNumber?.substring(9, 11)}"
        }
    }
}