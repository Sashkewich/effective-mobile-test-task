package com.example.effective_mobile_test_task.basket

import android.view.View
import com.example.effective_mobile_test_task.R
import com.example.effective_mobile_test_task.common.mvvm.BaseFragment

class BasketFragment : BaseFragment(R.layout.fragment_basket) {
    override fun bind() {

    }

    override fun initViews(view: View) {

    }
}