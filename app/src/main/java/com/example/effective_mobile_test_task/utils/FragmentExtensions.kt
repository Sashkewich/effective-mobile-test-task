package com.example.effective_mobile_test_task.utils

import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.example.effective_mobile_test_task.R
import com.example.effective_mobile_test_task.root.RootActivity
import com.google.android.material.bottomnavigation.BottomNavigationView

fun Fragment.replace(fragment: Fragment) {
    val fragmentManager = requireActivity().supportFragmentManager
    val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
    fragmentTransaction.addToBackStack(null)
        .replace(R.id.fragment_container, fragment)
        .commit()
}

fun Fragment.showNavigationBottomMenu(){
    val navigationMenuActivity = (activity as RootActivity).findViewById<BottomNavigationView>(R.id.bottom_navigation)
    navigationMenuActivity.visibility = View.VISIBLE
}

fun Fragment.hideNavigationBottomMenu(){
    val navigationMenuActivity = (activity as RootActivity).findViewById<BottomNavigationView>(R.id.bottom_navigation)
    navigationMenuActivity.visibility = View.GONE
}