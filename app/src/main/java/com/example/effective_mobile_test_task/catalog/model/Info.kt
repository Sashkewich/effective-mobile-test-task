package com.example.effective_mobile_test_task.catalog.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Info(
    @SerializedName("title")
    val title: String?,
    @SerializedName("value")
    val value: String?
) : Parcelable