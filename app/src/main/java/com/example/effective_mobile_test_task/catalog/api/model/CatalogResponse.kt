package com.example.effective_mobile_test_task.catalog.api.model


import com.google.gson.annotations.SerializedName

data class CatalogResponse(
    @SerializedName("items")
    val items: List<ItemResponse>
)