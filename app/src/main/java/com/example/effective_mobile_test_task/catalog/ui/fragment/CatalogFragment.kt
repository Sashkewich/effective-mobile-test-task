package com.example.effective_mobile_test_task.catalog.ui.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.PopupMenu
import android.widget.Spinner
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.effective_mobile_test_task.R
import com.example.effective_mobile_test_task.catalog.model.Item
import com.example.effective_mobile_test_task.catalog.ui.adapter.CatalogAdapter
import com.example.effective_mobile_test_task.catalog.ui.viewmodel.CatalogViewModel
import com.example.effective_mobile_test_task.common.mvvm.BaseFragment
import com.example.effective_mobile_test_task.databinding.FragmentCatalogBinding
import com.example.effective_mobile_test_task.databinding.FragmentCatalogItemBinding
import com.example.effective_mobile_test_task.utils.replace
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CatalogFragment : BaseFragment(R.layout.fragment_catalog) {
    private val binding: FragmentCatalogBinding by viewBinding()
    private val viewModel: CatalogViewModel by viewModels()
    private val adapter: CatalogAdapter by lazy {
        CatalogAdapter { item ->
            val catalogItemFragment = CatalogItemFragment()
            val args = Bundle()
            args.putString("available", item.available.toString())
            if (item.info[1]?.title == "Область использования" && item.info.size < 3){
                args.putString("article_title", item.info[0]?.title ?: "Артикул товара")
                args.putString("article_value", item.info[0]?.value ?: "Неизвестен")
                args.putString("uses_title", item.info[1]?.title)
                args.putString("uses_value", item.info[1]?.value)
            } else if (item.info[1]?.title == "Страна производства" && item.info.size < 3) {
                args.putString("article_title", item.info[0]?.title ?: "Артикул товара")
                args.putString("article_value", item.info[0]?.value ?: "Неизвестен")
                args.putString("country_maker_title", item.info[1]?.title)
                args.putString("country_maker_value", item.info[1]?.value)
            } else {
                args.putString("article_title", item.info[0]?.title ?: "Артикул товара")
                args.putString("article_value", item.info[0]?.value ?: "Неизвестен")
                args.putString("uses_title", item.info[1]?.title ?: "Область использования")
                args.putString("uses_value", item.info[1]?.value ?: "Неизвестно")
                args.putString("country_maker_title", item.info[2]?.title ?: "Страна производства")
                args.putString("country_maker_value", item.info[2]?.value ?: "Неизвестно")
            }

            args.putString("id", item.id)
            args.putString("description", item.description)
            args.putString("feedback_rating", item.feedback.rating.toString())
            args.putString("feedback_count", item.feedback.count.toString())
            args.putString("ingredients", item.ingredients)
            args.putString("price_without_discount", item.price.price)
            args.putString("price_with_discount", item.price.priceWithDiscount)
            args.putString("discount", item.price.discount.toString())
            args.putString("unit", item.price.unit)
            args.putString("ingredients", item.ingredients)
            args.putString("title", item.title)
            args.putString("subtitle", item.subtitle)
            catalogItemFragment.arguments = args
            replace(catalogItemFragment)
        }
    }


    override fun bind() {}

    override fun initViews(view: View) {
        with(binding) {
            catalogItemsRecyclerView.adapter = adapter
            catalogItemsRecyclerView.layoutManager = GridLayoutManager(context, 2)
            catalogItemsRecyclerView.setHasFixedSize(true)
            adapter.onAttachedToRecyclerView(catalogItemsRecyclerView)

            showCatalog(adapter)

            sortCatalogBtn.setOnClickListener {
                showSortPopupMenu(it)
            }
        }
    }

    private fun showCatalog(adapter: CatalogAdapter) {
        with(viewModel) {
            loadCatalog()

            observe(catalogStateFlow) { data ->
                adapter.sortByPopularity(data.items)
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun showSortPopupMenu(view: View) {
        val popupMenu = PopupMenu(context, view)
        popupMenu.menuInflater.inflate(R.menu.sort_menu, popupMenu.menu)

        popupMenu.setOnMenuItemClickListener { item: MenuItem ->
            with(viewModel) {
                when (item.itemId) {
                    R.id.sortByPopularity -> {
                        binding.sortCatalogBtn.text = getString(R.string.catalog_sort_famous_text)
                        observe(catalogStateFlow) { data ->
                            adapter.sortByPopularity(data.items)
                        }
                        true
                    }

                    R.id.sortByPriceDescending -> {
                        binding.sortCatalogBtn.text =
                            getString(R.string.catalog_sort_expensive_to_cheap_text)
                        observe(catalogStateFlow) { data ->
                            adapter.sortByPriceDescending(data.items)
                        }
                        true
                    }

                    R.id.sortByPrice -> {
                        binding.sortCatalogBtn.text =
                            getString(R.string.catalog_sort_cheap_to_expensive_text)
                        observe(catalogStateFlow) { data ->
                            adapter.sortByPriceAscending(data.items)
                        }
                        true
                    }

                    else -> false
                }
            }
        }
        popupMenu.show()
    }
}