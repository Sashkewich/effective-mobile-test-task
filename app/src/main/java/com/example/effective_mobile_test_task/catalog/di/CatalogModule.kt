package com.example.effective_mobile_test_task.catalog.di

import com.example.effective_mobile_test_task.catalog.api.CatalogApi
import com.example.effective_mobile_test_task.catalog.interactor.CatalogInteractor
import com.example.effective_mobile_test_task.catalog.repository.CatalogRepository
import com.example.effective_mobile_test_task.catalog.repository.CatalogRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class CatalogModule {
    @Binds
    @Singleton
    abstract fun bindRemoteRepository(repository: CatalogRepositoryImpl): CatalogRepository

    companion object {
        @Provides
        @Singleton
        fun provideApi(retrofit: Retrofit): CatalogApi =
            retrofit.create(CatalogApi::class.java)

        @Provides
        @Singleton
        fun provideInteractor(
            remoteRepository: CatalogRepositoryImpl
        ): CatalogInteractor =
            CatalogInteractor(remoteRepository)

        @IoDispatcher
        @Provides
        fun provideContextIo(): CoroutineDispatcher = Dispatchers.IO

        @DefaultDispatcher
        @Provides
        fun provideContextDefault(): CoroutineDispatcher = Dispatchers.Default

        @Retention(AnnotationRetention.RUNTIME)
        @Qualifier
        annotation class IoDispatcher

        @Retention(AnnotationRetention.RUNTIME)
        @Qualifier
        annotation class DefaultDispatcher
    }
}
