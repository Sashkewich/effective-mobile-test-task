package com.example.effective_mobile_test_task.catalog.api.model


import com.google.gson.annotations.SerializedName

data class PriceResponse(
    @SerializedName("discount")
    val discount: Int,
    @SerializedName("price")
    val price: String,
    @SerializedName("priceWithDiscount")
    val priceWithDiscount: String,
    @SerializedName("unit")
    val unit: String
)