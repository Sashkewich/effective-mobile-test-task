package com.example.effective_mobile_test_task.catalog.api.model


import com.google.gson.annotations.SerializedName

data class FeedbackResponse(
    @SerializedName("count")
    val count: Int,
    @SerializedName("rating")
    val rating: Double
)