package com.example.effective_mobile_test_task.catalog.ui.viewmodel

import androidx.lifecycle.viewModelScope
import com.example.effective_mobile_test_task.catalog.interactor.CatalogInteractor
import com.example.effective_mobile_test_task.catalog.model.Catalog
import com.example.effective_mobile_test_task.common.mvvm.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject
import kotlin.coroutines.cancellation.CancellationException

@HiltViewModel
class CatalogViewModel @Inject constructor(
    private val interactor: CatalogInteractor
) : BaseViewModel() {
    private val _catalogStateFlow = MutableStateFlow(
        Catalog(
            emptyList()
        )
    )

    val catalogStateFlow = _catalogStateFlow.asStateFlow()

    fun loadCatalog() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    _catalogStateFlow.tryEmit(interactor.getCatalog())
                } catch (e: CancellationException) {
                    Timber.e(e.message)
                } catch (t: Throwable) {
                    Timber.e(t.message)
                }
            }
        }
    }
}