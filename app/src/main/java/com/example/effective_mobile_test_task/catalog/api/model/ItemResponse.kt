package com.example.effective_mobile_test_task.catalog.api.model


import com.google.gson.annotations.SerializedName

data class ItemResponse(
    @SerializedName("available")
    val available: Int,
    @SerializedName("description")
    val description: String,
    @SerializedName("feedback")
    val feedback: FeedbackResponse,
    @SerializedName("id")
    val id: String,
    @SerializedName("info")
    val info: List<InfoResponse?>,
    @SerializedName("ingredients")
    val ingredients: String,
    @SerializedName("price")
    val price: PriceResponse,
    @SerializedName("subtitle")
    val subtitle: String,
    @SerializedName("tags")
    val tags: List<String>,
    @SerializedName("title")
    val title: String
)