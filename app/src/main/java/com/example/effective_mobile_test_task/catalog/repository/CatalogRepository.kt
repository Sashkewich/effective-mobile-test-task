package com.example.effective_mobile_test_task.catalog.repository

import com.example.effective_mobile_test_task.catalog.model.Catalog

interface CatalogRepository {
    suspend fun getCatalog(): Catalog
}