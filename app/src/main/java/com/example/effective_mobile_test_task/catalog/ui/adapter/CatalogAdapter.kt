package com.example.effective_mobile_test_task.catalog.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.effective_mobile_test_task.R
import com.example.effective_mobile_test_task.catalog.model.Item
import com.example.effective_mobile_test_task.databinding.CatalogItemBinding

class CatalogAdapter(private val clickOnItem: (Item) -> Unit) :
    RecyclerView.Adapter<CatalogAdapter.CatalogViewHolder>() {
    private val data = mutableListOf<Item>()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CatalogAdapter.CatalogViewHolder {
        val binding =
            CatalogItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CatalogViewHolder(binding)

    }

    override fun onBindViewHolder(holder: CatalogAdapter.CatalogViewHolder, position: Int) {
        val listItem = data[position]
        holder.onBind(listItem)
    }

    override fun getItemCount(): Int = data.size

    inner class CatalogViewHolder(private val binding: CatalogItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun onBind(item: Item) {
            var isFavorite = false

            with(binding) {
                priceWithoutDiscount.text = "${item.price.price} ₽"
                priceWithDiscount.text = "${item.price.priceWithDiscount} ₽"
                discount.text = "-${item.price.discount}%"
                itemName.text = item.title
                itemDescription.text = item.subtitle
                itemCountFeedback.text = "(${item.feedback.count})"
                itemRating.text = "${item.feedback.rating}"

                when (item.id) {
                    "cbf0c984-7c6c-4ada-82da-e29dc698bb50"
                    -> imageSlider.setImageResource(R.drawable.item_photo_6_full)

                    "54a876a5-2205-48ba-9498-cfecff4baa6e"
                    -> imageSlider.setImageResource(R.drawable.item_photo_2_full)

                    "75c84407-52e1-4cce-a73a-ff2d3ac031b3"
                    -> imageSlider.setImageResource(R.drawable.item_photo_3_full)

                    "16f88865-ae74-4b7c-9d85-b68334bb97db"
                    -> imageSlider.setImageResource(R.drawable.item_photo_5_full)

                    "26f88856-ae74-4b7c-9d85-b68334bb97db"
                    -> imageSlider.setImageResource(R.drawable.item_photo_1_full)

                    "15f88865-ae74-4b7c-9d81-b78334bb97db"
                    -> imageSlider.setImageResource(R.drawable.item_photo_6_full)

                    "88f88865-ae74-4b7c-9d81-b78334bb97db"
                    -> imageSlider.setImageResource(R.drawable.item_photo_4_full)

                    "55f58865-ae74-4b7c-9d81-b78334bb97db"
                    -> imageSlider.setImageResource(R.drawable.item_photo_2_full)
                }

                catalogItemLayout.setOnClickListener {
                    clickOnItem(item)
                }


                favoriteBtn.setOnClickListener {
                    isFavorite = when (isFavorite) {
                        false -> {
                            favoriteBtn.setImageResource(R.drawable.heart_active)
                            true
                        }

                        true -> {
                            favoriteBtn.setImageResource(R.drawable.heart)
                            false
                        }
                    }
                }


            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setItems(items: List<Item>) {
        data.clear()
        data.addAll(items)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun sortByPopularity(items: List<Item>) {
        data.clear()
        data.addAll(items)
        data.sortByDescending { it.feedback.rating }
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun sortByPriceDescending(items: List<Item>) {
        data.clear()
        data.addAll(items)
        data.sortByDescending { it.price.priceWithDiscount.toInt() }
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun sortByPriceAscending(items: List<Item>) {
        data.clear()
        data.addAll(items)
        data.sortBy { it.price.priceWithDiscount.toInt() }
        notifyDataSetChanged()
    }
}