package com.example.effective_mobile_test_task.catalog.interactor

import com.example.effective_mobile_test_task.catalog.model.Catalog
import com.example.effective_mobile_test_task.catalog.repository.CatalogRepositoryImpl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class CatalogInteractor @Inject constructor(
    private val remoteRepository: CatalogRepositoryImpl
) {
    suspend fun getCatalog(): Catalog =
        withContext(Dispatchers.IO){
            remoteRepository.getCatalog()
        }
}