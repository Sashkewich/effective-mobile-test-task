package com.example.effective_mobile_test_task.catalog.api

import com.example.effective_mobile_test_task.catalog.api.model.CatalogResponse
import retrofit2.http.GET

interface CatalogApi {
    @GET("97e721a7-0a66-4cae-b445-83cc0bcf9010")
    suspend fun getCatalog(): CatalogResponse
}