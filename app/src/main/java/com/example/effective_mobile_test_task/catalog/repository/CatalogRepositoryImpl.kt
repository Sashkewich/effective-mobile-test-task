package com.example.effective_mobile_test_task.catalog.repository

import com.example.effective_mobile_test_task.catalog.api.CatalogApi
import com.example.effective_mobile_test_task.catalog.model.Catalog
import com.example.effective_mobile_test_task.catalog.model.CatalogModelConverter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class CatalogRepositoryImpl @Inject constructor(
    private val api: CatalogApi
) : CatalogRepository {
    override suspend fun getCatalog(): Catalog =
        withContext(Dispatchers.IO) {
            CatalogModelConverter.fromNetwork(api.getCatalog())
        }
}