package com.example.effective_mobile_test_task.catalog.model

import com.example.effective_mobile_test_task.catalog.api.model.CatalogResponse
import com.example.effective_mobile_test_task.catalog.api.model.FeedbackResponse
import com.example.effective_mobile_test_task.catalog.api.model.InfoResponse
import com.example.effective_mobile_test_task.catalog.api.model.ItemResponse
import com.example.effective_mobile_test_task.catalog.api.model.PriceResponse

object CatalogModelConverter {
    fun fromNetwork(response: CatalogResponse): Catalog =
        Catalog(
            items = convertListItem(response.items)
        )

    private fun convertListItem(list: List<ItemResponse>): List<Item> =
        list.map { response ->
            Item(
                available = response.available,
                description = response.description,
                feedback = convertFeedback(response.feedback),
                id = response.id,
                info = convertListInfo(response.info),
                ingredients = response.ingredients,
                price = convertPrice(response.price),
                subtitle = response.subtitle,
                tags = response.tags,
                title = response.title
            )
        }

    private fun convertFeedback(response: FeedbackResponse): Feedback =
        Feedback(
            count = response.count,
            rating = response.rating
        )

    private fun convertListInfo(list: List<InfoResponse?>): List<Info?> =
        list.map { response ->
            Info(
                title = response?.title ?: "",
                value = response?.value ?: ""
            )
        }

    private fun convertPrice(response: PriceResponse): Price =
        Price(
            discount = response.discount,
            price = response.price,
            priceWithDiscount = response.priceWithDiscount,
            unit = response.unit
        )
}