package com.example.effective_mobile_test_task.catalog.ui.fragment

import android.annotation.SuppressLint
import android.view.Gravity.CENTER
import android.view.View
import android.widget.TextView
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.effective_mobile_test_task.R
import com.example.effective_mobile_test_task.catalog.model.Item
import com.example.effective_mobile_test_task.common.mvvm.BaseFragment
import com.example.effective_mobile_test_task.databinding.CustomLayoutBinding
import com.example.effective_mobile_test_task.databinding.FragmentCatalogItemBinding
import com.example.effective_mobile_test_task.utils.replace
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CatalogItemFragment : BaseFragment(R.layout.fragment_catalog_item) {
    private val binding: FragmentCatalogItemBinding by viewBinding()
    private val customLayoutBinding: CustomLayoutBinding by viewBinding()

    override fun bind() {}

    @SuppressLint("SetTextI18n")
    override fun initViews(view: View) {
        val id = arguments?.getString("id")
        val title = arguments?.getString("title")
        val subtitle = arguments?.getString("subtitle")
        val available = arguments?.getString("available")
        val fullDescription = arguments?.getString("description")
        val articleTitle = arguments?.getString("article_title")
        val articleValue = arguments?.getString("article_value")
        val priceWithDiscountValue = arguments?.getString("price_with_discount")
        val priceWithoutDiscountValue = arguments?.getString("price_without_discount")
        val discountValue = arguments?.getString("discount")
        val rubleUnit = arguments?.getString("unit")
        val feedbackRatingValue = arguments?.getString("feedback_rating")
        val feedbackCountValue = arguments?.getString("feedback_count")
        val ingredients = arguments?.getString("ingredients")
        val usesTitle = arguments?.getString("uses_title")
        val usesValue = arguments?.getString("uses_value")
        val countryMakerTitle = arguments?.getString("country_maker_title")
        val countryMakerValue = arguments?.getString("country_maker_value")

        with(binding) {
            backButton.setOnClickListener {
                replace(CatalogFragment())
            }
        }

        with(customLayoutBinding) {
            showItemImage(id)

            var isFavorite = false

            favoriteBtn.setOnClickListener {
                isFavorite = when (isFavorite) {
                    false -> {
                        favoriteBtn.setImageResource(R.drawable.heart_active)
                        true
                    }

                    true -> {
                        favoriteBtn.setImageResource(R.drawable.heart)
                        false
                    }
                }
            }

            itemHideDescription.setOnClickListener {
                showOrHideDescription()
            }

            itemNameBrand.text = title
            itemFullname.text = subtitle
            itemAvailable.text = "Доступно для заказа $available шт."
            itemFullDescription.text = fullDescription

            itemArticle.text = articleTitle
            itemArticleNumber.text = articleValue

            itemUsesTitle.text = usesTitle ?: "Область применения"
            itemUsesValue.text = usesValue ?: "Неизвестно"

            itemCountryMakerTitle.text = countryMakerTitle ?: "Страна производства"
            itemCountryMakerValue.text = countryMakerValue ?: "Неизвестно"

            itemRating.text = feedbackRatingValue
            itemCountFeedback.text = pluralizeReviews(feedbackCountValue!!.toInt())

            priceWithDiscount.text = "$priceWithDiscountValue $rubleUnit"
            priceWithoutDiscount.text = "$priceWithoutDiscountValue $rubleUnit"
            discount.text = "-$discountValue%"

            itemCompositionValue.text = ingredients
            itemBrandButton.text = title

            leftPriceWithDiscount.text = "$priceWithDiscountValue $rubleUnit"
            rightPriceWithoutDiscount.text = "$priceWithoutDiscountValue $rubleUnit"

            // Проверяем, помещается ли весь текст в две строки
            if (isMultiLineText(itemCompositionValue)) {
                itemShowMoreComposition.visibility = View.VISIBLE
            }

            itemShowMoreComposition.setOnClickListener {
                if (itemShowMoreComposition.text == "Подробнее") {
                    showAllIngredients()
                } else showLimitedIngredients()
            }
        }
    }

    private fun pluralizeReviews(count: Int): String {
        return when {
            count % 10 == 1 && count % 100 != 11 -> "$count отзыв"
            count % 10 in 2..4 && count % 100 !in 12..14 -> "$count отзыва"
            else -> "$count отзывов"
        }
    }

    private fun showAllIngredients() {
        with(customLayoutBinding) {
            itemCompositionValue.maxLines = Integer.MAX_VALUE
            itemShowMoreComposition.text = "Скрыть"
        }
    }

    private fun showLimitedIngredients() {
        with(customLayoutBinding) {
            itemCompositionValue.maxLines = 2
            itemShowMoreComposition.text = "Подробнее"
        }
    }

    private fun isMultiLineText(textView: TextView): Boolean {
        val layout = textView.layout ?: return false
        val lineCount = layout.lineCount
        return lineCount > 2
    }

    private fun showOrHideDescription() {
        with(customLayoutBinding) {
            when (itemHideDescription.text) {
                "Скрыть" -> {
                    itemHideDescription.text = "Подробнее"
                    itemFullDescription.visibility = View.GONE
                    itemBrandButton.visibility = View.GONE
                }

                "Подробнее" -> {
                    itemHideDescription.text = "Скрыть"
                    itemFullDescription.visibility = View.VISIBLE
                    itemBrandButton.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun showItemImage(id: String?){
        with(customLayoutBinding){
            when(id){
                "cbf0c984-7c6c-4ada-82da-e29dc698bb50"
                -> itemImageFull.setImageResource(R.drawable.item_photo_6_full)

                "54a876a5-2205-48ba-9498-cfecff4baa6e"
                -> itemImageFull.setImageResource(R.drawable.item_photo_2_full)

                "75c84407-52e1-4cce-a73a-ff2d3ac031b3"
                -> itemImageFull.setImageResource(R.drawable.item_photo_3_full)

                "16f88865-ae74-4b7c-9d85-b68334bb97db"
                -> itemImageFull.setImageResource(R.drawable.item_photo_5_full)

                "26f88856-ae74-4b7c-9d85-b68334bb97db"
                -> itemImageFull.setImageResource(R.drawable.item_photo_1_full)

                "15f88865-ae74-4b7c-9d81-b78334bb97db"
                -> itemImageFull.setImageResource(R.drawable.item_photo_6_full)

                "88f88865-ae74-4b7c-9d81-b78334bb97db"
                -> itemImageFull.setImageResource(R.drawable.item_photo_4_full)

                "55f58865-ae74-4b7c-9d81-b78334bb97db"
                -> itemImageFull.setImageResource(R.drawable.item_photo_2_full)
            }
        }
    }
}