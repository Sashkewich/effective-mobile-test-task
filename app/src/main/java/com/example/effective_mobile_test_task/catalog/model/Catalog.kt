package com.example.effective_mobile_test_task.catalog.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Catalog(
    @SerializedName("items")
    val items: List<Item>
) : Parcelable