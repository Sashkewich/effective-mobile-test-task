package com.example.effective_mobile_test_task.catalog.api.model


import com.google.gson.annotations.SerializedName

data class InfoResponse(
    @SerializedName("title")
    val title: String?,
    @SerializedName("value")
    val value: String?
)