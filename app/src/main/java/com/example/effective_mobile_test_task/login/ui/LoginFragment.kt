package com.example.effective_mobile_test_task.login.ui

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.Toast
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.effective_mobile_test_task.R
import com.example.effective_mobile_test_task.common.mvvm.BaseFragment
import com.example.effective_mobile_test_task.databinding.FragmentLoginBinding
import com.example.effective_mobile_test_task.main_page.ui.MainFragment
import com.example.effective_mobile_test_task.utils.hideNavigationBottomMenu
import com.example.effective_mobile_test_task.utils.replace

class LoginFragment : BaseFragment(R.layout.fragment_login) {
    private val binding: FragmentLoginBinding by viewBinding()

    override fun bind() {}

    override fun initViews(view: View) {
        with(binding) {
            hideNavigationBottomMenu()

            editTextFirstname.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

                override fun afterTextChanged(s: Editable?) {
                    validateEditText(editTextFirstname)
                }
            })

            editTextSurname.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

                override fun afterTextChanged(s: Editable?) {
                    validateEditText(editTextSurname)
                }
            })

            editTextTelephone.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

                override fun afterTextChanged(s: Editable?) {
                    validatePhoneNumberEditText()
                }
            })

            loginButton.setOnClickListener {
                // Проверяем, все ли поля заполнены и валидны
                if (editTextFirstname.text.isNotBlank() &&
                    editTextSurname.text.isNotBlank() && editTextTelephone.text.isNotBlank()
                ) {
                    // Инициализируем sharedPreferences
                    val sharedPreference =
                        activity?.getSharedPreferences(
                            "LOGIN_PREF",
                            Context.MODE_PRIVATE
                        )

                    // Начинаем его редактировать
                    val editor = sharedPreference?.edit()
                    val firstName =
                        binding.editTextFirstname.text.toString() // Хватаем имя пользователя из EditText
                    val lastName =
                        binding.editTextSurname.text.toString() // Хватаем фамилию пользователя из EditText

                    val phoneNumber =
                        binding.editTextTelephone.text.toString() // Хватаем фамилию пользователя из EditText
                    editor?.putString(
                        "FIRSTNAME",
                        firstName
                    ) // Передаем в sharedPreferences firstname
                    editor?.putString("LASTNAME", lastName) // Передаем в sharedPreferences lastName
                    editor?.putString(
                        "PHONE_NUMBER",
                        phoneNumber
                    ) // Передаем в sharedPreferences phoneNumber
                    editor?.apply() // Применяем изменения
                    replace(MainFragment())
                }
            }
        }
    }

    private fun validateEditText(editText: EditText) {
        val text = editText.text.toString()
        if (!text.matches(Regex("[а-яА-Я]+")) && text.isNotEmpty()) {
            editText.setBackgroundColor(resources.getColor(R.color.red))
        } else {
            editText.setBackgroundColor(resources.getColor(R.color.light_grey))
        }
        validateLoginButton()
    }

    private fun validatePhoneNumberEditText() {
        validateLoginButton()
    }

    private fun validateLoginButton() {
        with(binding) {
            if (editTextFirstname.text.matches(Regex("[а-яА-Я]+")) &&
                editTextFirstname.text.isNotBlank() &&
                editTextSurname.text.matches(Regex("[а-яА-Я]+")) &&
                editTextSurname.text.isNotBlank() &&
                editTextTelephone.text.startsWith("7") && editTextTelephone.text.length == 11
            ) {
                loginButton.isClickable = true
                loginButton.setBackgroundColor(resources.getColor(R.color.red))
            } else {
                loginButton.isClickable = false
                loginButton.setBackgroundColor(resources.getColor(R.color.light_pink))
            }
        }
    }
}